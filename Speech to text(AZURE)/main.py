import azure.cognitiveservices.speech as speechsdk


def from_mic():
    speech_config = speechsdk.SpeechConfig(subscription="5c079b8b459b4586a7443d0bcbccf4e3",
                                           region="eastus")
    speech_recognizer = speechsdk.SpeechRecognizer(speech_config=speech_config)

    print("Speak into your microphone.")
    result = speech_recognizer.recognize_once_async().get()
    print(result.text)


from_mic()
