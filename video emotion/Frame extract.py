import argparse
import cv2


def extractimages(pathin, pathout):
    count = 0
    vidcap = cv2.VideoCapture(pathin)
    success, image = vidcap.read()
    while success:
        vidcap.set(cv2.CAP_PROP_POS_MSEC, (count*3000))
        success, image = vidcap.read()
        print('Read a new frame: ', success)
        cv2.imwrite(pathout + "\\%d.jpeg" % count, image)
        count = count + 1


# python a1.py --pathin "C:\Users\hp-p\Desktop\Gopi\video frame extract\3.mp4" --pathout
# "C:\Users\hp-p\Desktop\Gopi\video frame extra
# ct\data"

if __name__ == "__main__":
    a = argparse.ArgumentParser()
    a.add_argument("--pathin", help="path to video")
    a.add_argument("--pathout", help="path to images")
    args = a.parse_args()
    extractimages(args.pathin, args.pathout)
