import requests
import json
import  glob
import os

subscription_key = '2517c0bd20a749fb94aeb9c268243b05'
uri_base = 'https://facailemotiondetect.cognitiveservices.azure.com/'
headers = {
    'Content-Type': 'application/octet-stream',
    'Ocp-Apim-Subscription-Key': subscription_key,
}
params = {
    'returnFaceId': 'false',
    'returnFaceLandmarks': 'false',
    'returnFaceRectangle': 'false',
    'returnFaceAttributes': 'age,gender,emotion',
}
IMAGES_FOLDER='C:/Users/hp-p/Desktop/Gopi/video frame extract/data'
test_image_array = glob.glob(os.path.join(IMAGES_FOLDER, '*jpeg'))
count = 0

for img_path in test_image_array:
    body = open(img_path, 'rb').read()
    try:
        response = requests.request('POST', uri_base + '/face/v1.0/detect', data=body, headers=headers, params=params)
        print('Response:')
        parsed = json.loads(response.text)
        print(json.dumps(parsed, sort_keys=True, indent=2))
    except Exception as e:
        print('Error:')
        print(e)
