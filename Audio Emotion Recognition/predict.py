import librosa
import soundfile
import os
import glob
import pickle
import numpy as np


def extract_feature1(file_name, mfcc, chroma, mel):
    with soundfile.SoundFile(file_name) as sound_file:
        X = sound_file.read(dtype="float32")
        sample_rate=sound_file.samplerate
        if chroma:
            stft=np.abs(librosa.stft(X))
        result=np.array([])
        if mfcc:
            mfccs=np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=30).T, axis=0)
            result=np.hstack((result, mfccs))
        if chroma:
            chroma=np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
            result=np.hstack((result, chroma))
        if mel:
            mel=np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
            result=np.hstack((result, mel))
    return result


def load_data(filename):
    x = []
    for file in glob.glob(filename):
        file_name = os.path.basename(file)
        feature = extract_feature1(file, mfcc=True, chroma=True, mel=True)
        x.append(feature)
    return x


filename=input("Enter the model name:")
model=pickle.load(open("Models/"+filename, "rb"))

dataset_path = 'C:/Users/hp-p/Desktop/Gopi/Audio Emotion Recognition/output\\'
files = glob.glob(os.path.join(dataset_path, '*wav'))
for count, filename in enumerate(files):
    x_test=load_data(""+filename)
    y_pred=model.predict(x_test)
    print(count, y_pred)
