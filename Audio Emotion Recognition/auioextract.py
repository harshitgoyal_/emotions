from pydub import AudioSegment
from pydub.utils import make_chunks


myaudio = AudioSegment.from_file("3.wav", "wav")
chunk_length_ms = 3000
chunks = make_chunks(myaudio, chunk_length_ms)
for i, chunk in enumerate(chunks):
    chunk_name = "{0}.wav".format(i)
    print("exporting", chunk_name)
    chunk.export(f"C:/Users/hp-p/Desktop/Gopi/Audio Emotion Recognition/output/{chunk_name}", format="wav")
