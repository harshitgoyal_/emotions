import librosa
import soundfile
import os
import numpy as np
# import pickle
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
from catboost import CatBoostClassifier


def extract_feature(file_name, mfcc, chroma, mel):
    with soundfile.SoundFile(file_name) as sound_file:
        X = sound_file.read(dtype="float64")
        sample_rate = sound_file.samplerate
        if chroma:
            stft = np.abs(librosa.stft(X))
        result = np.array([])
        if mfcc:
            mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=39).T, axis=0)
            result = np.hstack((result, mfccs))
        if chroma:
            chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
            result = np.hstack((result, chroma))
        if mel:
            mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
            result = np.hstack((result, mel))
    return result


emotions = {
  '01': 'neutral',
  '02': 'calm',
  '03': 'happy',
  '04': 'sad',
  '05': 'angry',
  '06': 'fearful',
  '07': 'disgust',
  '08': 'surprised'
}

observed_emotions=['calm', 'happy', 'neutral', 'sad', 'angry', 'surprised']


def load_data(test_size=0.2):
    x, y = [], []
    dataset_path = "C:/Users/hp-p/Desktop/Gopi/Audio Emotion Recognition/dataset/"
    files = os.listdir(dataset_path)
    action_list = []
    for file in files:
        if "actor" in str(file).lower():
            action_list.append(dataset_path + file)
    # print("ACTION LIST", action_list)
    audio_data_list = []
    for action_f in action_list:
        audios = os.listdir(action_f)
        for audio in audios:
            audio_data_list.append(action_f + "/" + audio)
    # print(audio_data_list)
    for file in audio_data_list:
        file_name = os.path.basename(file)
        emotion = emotions[file_name.split("-")[2]]
        print("File name = {} , emotion = {}".format(file_name, emotion))
        if emotion not in observed_emotions:
            continue
        feature = extract_feature(file, mfcc=True, chroma=True, mel=True)
        x.append(feature)
        y.append(emotion)
    return train_test_split(np.array(x), y, test_size=test_size, random_state=6)


x_train, x_test, y_train, y_test = load_data(test_size=0.20)
print(x_train)
print("\n")
print(x_test)
print((x_train.shape[0], x_test.shape[0]))
print(f'Features extracted: {x_train.shape[1]}')

# MLPClassifier
# model = MLPClassifier(alpha=0.01, batch_size=10, epsilon=1e-08, hidden_layer_sizes=(924,),
#                       learning_rate=0.3, max_iter=850)
# model.fit(x_train, y_train)
# y_pred = model.predict(x_test)
# accuracy = accuracy_score(y_true=y_test, y_pred=y_pred)
# print("Accuracy: {:.2f}%".format(accuracy*100))

# CatBoostClassifier
# model = CatBoostClassifier(iterations=4000, learning_rate=0.3, depth=4)
# model.fit(x_train, y_train)
# y_pred = model.predict(x_test)
# accuracy = accuracy_score(y_true=y_test, y_pred=y_pred)
# print("Accuracy: {:.2f}%".format(accuracy*100))
