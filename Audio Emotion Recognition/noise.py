import soundfile as sf
import math
import numpy as np
import librosa

filename = "C:/Users/hp-p/Desktop/Gopi/Audio Emotion Recognition/output/0.wav"
librosa_audio, librosa_sample_rate = librosa.load(filename)
SNR = 1
signal, sr = librosa.load("C:/Users/hp-p/Desktop/Gopi/Audio Emotion Recognition/data_noise.wav")
RMS_s = math.sqrt(np.mean(signal**2))
RMS_n = math.sqrt(RMS_s**2/(pow(10, SNR/10)))
STD_n = RMS_n
noise = np.random.normal(0, STD_n, librosa_audio.shape[0])

# Reducing noise and amplifing the audio
final_audio = (librosa_audio*10 + noise/30)

filename = 'C:/Users/hp-p/Desktop/Gopi/1.wav'
sf.write(filename, final_audio, 22050)
